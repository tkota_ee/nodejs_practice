
// const EventEmitter = require('events');
// // const emitter = new EventEmitter();

// // Register a listener
// const Logger = require('./logger');
// const logger = new Logger();

// logger.on('messageLogged', (arg) => {
//     console.log('Listerner called', arg);
// })

// logger.log('message');
// Raise an event
// emitter.emit('messageLogged', {id: 1, url: 'http://' });
// Making a noise, produce - signaling

// Raise: logging (data: message)
const http = require('http');

const server = http.createServer((req, res) => {
    if (req.url == '/') {
        res.write('Hello world');
        res.end();
    }

    if (req.url === '/api/courses') {
        res.write(JSON.stringify([1, 2, 3]));
        res.end();
    }
});

// server.on('connection', (socket) => {
//     console.log('New connection....')
// });
server.listen(3000);


console.log('Listening on port 3000...');

